#include "vespaShapeSmoothingCLP.h"

#include <iostream>

#include <vtkCGALShapeSmoothing.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLPolyDataWriter.h>

extern "C" 
{

int ModuleEntryPoint(int argc, char* argv[])
{
  PARSE_ARGS

  vtkNew<vtkXMLPolyDataReader> polyReader;
  polyReader->SetFileName(poly.c_str());

  vtkNew<vtkCGALShapeSmoothing> shapeSmoothing;
  shapeSmoothing->SetInputConnection(polyReader->GetOutputPort());
  shapeSmoothing->SetTimeStep(timeStep);
  shapeSmoothing->SetNumberOfIterations(numberOfIterations);
 
  vtkNew<vtkXMLPolyDataWriter> writer;
  writer->SetFileName(output.c_str());
  writer->SetInputConnection(shapeSmoothing->GetOutputPort());
 
  return writer->Write() ? 0 : 1;
}

}