#include "vespaMeshSubdivisionCLP.h"

#include <vtkCGALMeshSubdivision.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLPolyDataWriter.h>

extern "C" 
{

static int CGALMeshSubdivisionFromString(const std::string& str)
{
  if (str == "CatmullClark")
  {
    return vtkCGALMeshSubdivision::CATMULL_CLARK;
  }
  else if (str == "Loop")
  {
    return vtkCGALMeshSubdivision::LOOP;
  }
  else if (str == "DooSabin")
  {
    return vtkCGALMeshSubdivision::DOO_SABIN;
  }
  else if (str == "Sqrt3")
  {
    return vtkCGALMeshSubdivision::SQRT3;
  }

  return -1;
}

int ModuleEntryPoint(int argc, char* argv[])
{
  PARSE_ARGS

  vtkNew<vtkXMLPolyDataReader> polyReader;
  polyReader->SetFileName(poly.c_str());

  vtkNew<vtkCGALMeshSubdivision> meshSubdivision;
  meshSubdivision->SetInputConnection(polyReader->GetOutputPort());
  meshSubdivision->SetSubdivisionType(CGALMeshSubdivisionFromString(subdivisionType));
  meshSubdivision->SetNumberOfIterations(numberOfIterations);
 
  vtkNew<vtkXMLPolyDataWriter> writer;
  writer->SetFileName(output.c_str());
  writer->SetInputConnection(meshSubdivision->GetOutputPort());
 
  return writer->Write() ? 0 : 1;
}

}