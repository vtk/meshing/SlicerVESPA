set(proj Eigen)

# Set dependency list
set(${proj}_DEPENDS
    ""
)

# Include dependent projects if any
ExternalProject_Include_Dependencies(${proj} PROJECT_VAR proj)

if(${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj})
    message(FATAL_ERROR "Enabling ${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj} is not supported !")
endif()

# Sanity checks
if(DEFINED ${proj}_INCLUDE_DIR AND NOT EXISTS ${${proj}_INCLUDE_DIR})
    message(FATAL_ERROR "${proj}_INCLUDE_DIR [${${proj}_INCLUDE_DIR}] variable is defined but corresponds to nonexistent directory")
endif()

if(NOT DEFINED ${proj}_INCLUDE_DIR AND NOT ${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj})
    ExternalProject_Add(${proj}
        ${${proj}_EP_ARGS}
        
        GIT_REPOSITORY https://gitlab.com/libeigen/eigen.git
        GIT_TAG 3.4.0

        SOURCE_DIR ${CMAKE_BINARY_DIR}/${proj}
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
        DEPENDS
            ${${proj}_DEPENDS}
    )

    set(EIGEN3_INCLUDE_DIR ${CMAKE_BINARY_DIR}/${proj})
else()
    ExternalProject_Add_Empty(${proj} DEPENDS ${${proj}_DEPENDS})
endif()

mark_as_superbuild(EIGEN3_INCLUDE_DIR)
