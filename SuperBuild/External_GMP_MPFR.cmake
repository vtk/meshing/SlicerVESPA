set(proj GMP_MPFR)

# Set dependency list
set(${proj}_DEPENDS
    ""
)

# Include dependent projects if any
ExternalProject_Include_Dependencies(${proj} PROJECT_VAR proj)

if(${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj})
    message(FATAL_ERROR "Enabling ${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj} is not supported !")
endif()

if("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
    ExternalProject_Add(${proj}
        URL https://github.com/CGAL/cgal/releases/download/v5.5.1/CGAL-5.5.1-win64-auxiliary-libraries-gmp-mpfr.zip
        URL_MD5 247f4dca741c6b9a9be76286414070fa
        SOURCE_DIR ${CMAKE_CURRENT_BINARY_DIR}/${proj}
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
        DEPENDS
            ${${proj}_DEPENDS}
    )

    set(GMP_INCLUDE_DIR ${CMAKE_CURRENT_BINARY_DIR}/${proj}/gmp/include)
    set(GMP_LIBRARY_DEBUG ${CMAKE_CURRENT_BINARY_DIR}/${proj}/gmp/lib/libgmp-10.lib)
    set(GMP_LIBRARY_RELEASE ${CMAKE_CURRENT_BINARY_DIR}/${proj}/gmp/lib/libgmp-10.lib)
    set(GMP_LIBRARIES ${CMAKE_CURRENT_BINARY_DIR}/${proj}/gmp/lib/libgmp-10.lib)
    set(MPFR_INCLUDE_DIR ${CMAKE_CURRENT_BINARY_DIR}/${proj}/gmp/include)
    set(MPFR_LIBRARIES ${CMAKE_CURRENT_BINARY_DIR}/${proj}/gmp/lib/libmpfr-4.lib)
    set(MPFR_LIBRARIES_DIR ${CMAKE_CURRENT_BINARY_DIR}/${proj}/gmp/lib)
else()
    # Hardcoded so names: may need to be changed in case of source update
    # file(REAL_PATH) (CMake 3.19+) to deref symlink ?
    cmake_host_system_information(RESULT processor_count QUERY NUMBER_OF_LOGICAL_CORES)

    # GMP
    set(gmp_so_name libgmp.so.10.4.1)
    set(gmp_install_dir ${CMAKE_CURRENT_BINARY_DIR}/gmp-install)

    # Build GMP
    ExternalProject_Add(GMP
        URL https://gmplib.org/download/gmp/gmp-6.2.1.tar.lz
        URL_MD5 03a31d8cbaf29d136252f8f38875ed82
        SOURCE_DIR ${CMAKE_CURRENT_BINARY_DIR}/gmp
        BUILD_IN_SOURCE 1
        CONFIGURE_COMMAND ./configure --prefix=${gmp_install_dir}
        BUILD_COMMAND make -j${processor_count}
        INSTALL_COMMAND make install
    )

    set(GMP_INCLUDE_DIR ${gmp_install_dir}/include)
    set(GMP_LIBRARIES ${gmp_install_dir}/lib/${gmp_so_name})
    set(GMP_LIBRARY_DEBUG ${GMP_LIBRARIES})
    set(GMP_LIBRARY_RELEASE ${GMP_LIBRARIES})

    # MPFR
    set(mpfr_so_name libmpfr.so.6.2.0)
    set(mpfr_install_dir ${CMAKE_CURRENT_BINARY_DIR}/mpfr-install)
    
    # Use environment variables to force mpfr.so to have $ORIGIN in its RPATH so it can be relocated and still find GMP
    # ORIGIN='$ORIGIN' and '$$ORIGIN' are required to have "$ORIGIN" in the final elf file due to both bash and makefile rules
    set(mpfr_configure_env ORIGIN='$ORIGIN' LDFLAGS=-Wl,-rpath,'$$ORIGIN')
    set(mpfr_configure_args --with-gmp=${gmp_install_dir} --prefix=${mpfr_install_dir})

    # Build MPFR using previous GMP build and install
    ExternalProject_Add(MPFR
        URL https://www.mpfr.org/mpfr-current/mpfr-4.2.0.tar.xz
        URL_MD5 a25091f337f25830c16d2054d74b5af7
        SOURCE_DIR ${CMAKE_CURRENT_BINARY_DIR}/mpfr
        BUILD_IN_SOURCE 1
        CONFIGURE_COMMAND ${CMAKE_COMMAND} -E env ${mpfr_configure_env} ./configure ${mpfr_configure_args}
        BUILD_COMMAND make -j${processor_count}
        INSTALL_COMMAND make install
        DEPENDS
            GMP
    )

    set(MPFR_INCLUDE_DIR ${mpfr_install_dir}/include)
    set(MPFR_LIBRARIES ${mpfr_install_dir}/lib/${mpfr_so_name})
    set(MPFR_LIBRARIES_DIR ${mpfr_install_dir}/lib)

    # Don't create imported targets because we won't use it in our code
    # Dummy project for dependencies
    ExternalProject_Add_Empty(${proj} DEPENDS GMP MPFR)
endif()

mark_as_superbuild(GMP_INCLUDE_DIR)
mark_as_superbuild(GMP_LIBRARY_DEBUG)
mark_as_superbuild(GMP_LIBRARY_RELEASE)
mark_as_superbuild(GMP_LIBRARIES)
mark_as_superbuild(MPFR_INCLUDE_DIR)
mark_as_superbuild(MPFR_LIBRARIES)
mark_as_superbuild(MPFR_LIBRARIES_DIR)